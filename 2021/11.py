from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def __init__(self, day) -> None:
        super().__init__(day)
        self.input = [[int(d) for d in line] for line in self.input]

    class Octopus():
        def __init__(self, coordinates, energy_level) -> None:
            self.energy_level = energy_level
            self.x = coordinates[0]
            self.y = coordinates[1]
            self.flashed = False

        def __repr__(self) -> str:
            return str(self.energy_level)

        def __int__(self) -> int:
            return self.energy_level

        def increase_energy(self):
            self.energy_level += 1

        def flash(self, octopi):
            self.flashed = True
            for x_diff in [-1, 0, 1]:
                for y_diff in [-1, 0, 1]:
                    if (x_diff != 0 or y_diff != 0) and octopi.get(
                        (self.x + x_diff, self.y + y_diff)
                    ) is not None:
                        octopi.get((self.x + x_diff, self.y + y_diff)).increase_energy()

    def _create_octopi(self):
        octopi = {}
        for y_idx, row in enumerate(self.input):
            for x_idx, val in enumerate(row):
                octopi[(x_idx, y_idx)] = self.Octopus((x_idx, y_idx),val)
        return octopi

    def print_grid(self, grid_size=10):
        row = [0 for _ in range(grid_size)]
        grid = [row.copy() for _ in range(grid_size)]
        for coord, energy in self.octopi.items():
            grid[coord[1]][coord[0]] = str(energy).rjust(2, ' ' )
        for r in grid:
            print(r)
        print("\n")

    def increase_energy_phase(self):
        for octopus_coordinate in self.octopi.keys():
                self.octopi[octopus_coordinate].increase_energy()

    def flash_phase(self, flash_counter):
        flash_registered = True
        while flash_registered:
            flash_registered = False
            for octopus_coordinate in self.octopi.keys():
                if (self.octopi[octopus_coordinate].energy_level > 9
                    and not self.octopi[octopus_coordinate].flashed
                ):
                    self.octopi[octopus_coordinate].flash(self.octopi)
                    flash_counter += 1
                    flash_registered = True
        return flash_counter

    def reset_phase(self):
        for octopus_coordinate in self.octopi.keys():
            if self.octopi[octopus_coordinate].energy_level > 9:
                self.octopi[octopus_coordinate].energy_level = 0
                self.octopi[octopus_coordinate].flashed = False

    def part_one(self):
        self.octopi = self._create_octopi()
        flash_counter = 0
        for _ in range(100):
            # Step 1: Increase energy by 1
            self.increase_energy_phase()
            # Step 2: Flash
            flash_counter = self.flash_phase(flash_counter)
            # Step 3: Reset octopi
            self.reset_phase()
        return flash_counter

    def part_two(self):
        self.octopi = self._create_octopi()
        for iteration in range(1, 999):
            # Step 1: Increase energy by 1
            self.increase_energy_phase()
            # Step 2: Flash
            _ = self.flash_phase(flash_counter=0)
            # Step 3: Reset octopi
            self.reset_phase()
            # Step 4: Check if in sync
            if sum(int(i) for i in list(self.octopi.values())) == 0:
                return iteration


RiddleSolver(11).solve()
