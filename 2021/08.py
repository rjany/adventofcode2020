from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):

    def __init__(self, day) -> None:
        super().__init__(day)
        self.input = self._format_input()

    def _format_input(self):
        formatted_input = []
        for line in self.input:
            pattern, output = line.split("|")
            pattern = pattern.split(" ")
            output = output.split(" ")
            sorted_pattern, sorted_output = ([], [])
            # sort content
            for pat in pattern:
                if pat != "":
                    sorted_pattern.append(self._sort_string(pat))
            for out in output:
                if out != "":
                    sorted_output.append(self._sort_string(out))
            formatted_input.append((set(sorted_pattern), sorted_output))
        return formatted_input

    def _sort_string(self, string):
        l_string = list(string)
        l_string.sort()
        return "".join(l_string)

    def part_one(self):
        counter = 0
        for line in self.input:
            for digit in line[1]:
                if len(digit) in [2, 3, 4, 7]:
                    counter += 1
        return counter

    def part_two(self):
        sum = 0
        for pattern, value in self.input:
            mapping = {}
            # Get 1, 4, 7 and 8
            for num in pattern.copy():
                if len(num) == 2:
                    mapping[1] = num
                    pattern.remove(num)
                if len(num) == 3:
                    mapping[7] = num
                    pattern.remove(num)
                if len(num) == 4:
                    mapping[4] = num
                    pattern.remove(num)
                if len(num) == 7:
                    mapping[8] = num
                    pattern.remove(num)
            # Get 3 and 9
            for num in pattern.copy():
                if len(num) == 5 and not set(mapping[7]) - set(num):
                    mapping[3] = num
                    pattern.remove(num)
                if not set(mapping[4]) - set(num):
                    mapping[9] = num
                    pattern.remove(num)
            # Get 0 and 5
            for num in pattern.copy():
                if not set(mapping[7]) - set(num) and len(num) == 6:
                    mapping[0] = num
                    pattern.remove(num)
                if not set(num) - set(mapping[9]):
                    mapping[5] = num
                    pattern.remove(num)
            # Get 2 and 6
            for num in pattern.copy():
                if len(num) == 5:
                    mapping[2] = num
                if len(num) == 6:
                    mapping[6] = num
            inv_map = {v: k for k, v in mapping.items()}
            str_value = "".join(str(inv_map[val]) for val in value)
            sum += int(str_value)
        return sum


RiddleSolver(8).solve()
