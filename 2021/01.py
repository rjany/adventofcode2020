from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        lines = [int(line) for line in self.input]
        return sum(lines[i] > lines[i-1] for i in range(1, len(lines)))

    def part_two(self):
        lines = [int(line) for line in self.input]
        return sum(
            sum(lines[i : i + 3]) > sum(lines[i - 1 : i + 2])
            for i in range(1, len(lines) - 2)
        )


RiddleSolver(1).solve()
