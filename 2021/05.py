from aoc_wrapper import BaseRiddleSolver


def parse_input(line):
    temp = line.split(" ")
    start = (int(temp[0].split(",")[0]), int(temp[0].split(",")[1]))
    end = (int(temp[2].split(",")[0]), int(temp[2].split(",")[1]))
    return start, end

class Board():
    def __init__(self, lines) -> None:
        self.lines = lines
        self.max_x , self.max_y = self._get_board_size()
        self.board = self._init_board()

    def _get_board_size(self):
        max_x = 0
        max_y = 0
        for line in self.lines:
            if line.start[0] > max_x:
                max_x = line.start[0]
            if line.end[0] > max_x:
                max_x = line.end[0]
            if line.start[1] > max_y:
                max_y = line.start[1]
            if line.end[1] > max_y:
                max_y = line.end[1]
        return max_x, max_y

    def _init_board(self):
        board = {}
        for x in range(self.max_x + 1):
            for y in range(self.max_y + 1):
                board[(x,y)] =  0
        return board

    def draw_lines(self):
        for line in self.lines:
            x_range = list(
                range(
                    line.start[0], line.end[0]+1
                ) if line.end[0] >= line.start[0] else range(line.start[0], line.end[0]-1, -1)
            )
            y_range = list(
                range(
                    line.start[1], line.end[1]+1
                ) if line.end[1] >= line.start[1] else range(line.start[1], line.end[1]-1, -1)
            )
            if len(y_range) == 1:
                y_range *= len(x_range)
            if len(x_range) == 1:
                x_range *= len(y_range)

            for idx in range(len(x_range)):
                self.board[(x_range[idx], y_range[idx])] += 1

    def print(self):
        ret = ""
        for y in range(self.max_y + 1):
            for x in range(self.max_x + 1):
                ret += str(self.board[(x, y)]) if self.board[(x, y)] != 0 else "."
            ret += "\n"
        print(ret)

    def count_overlap(self, min_overlap):
        return sum(
            overlaps >= min_overlap for field, overlaps in self.board.items()
        )


class Line():
    def __init__(self, start, end) -> None:
        self.start = start # i.e. (0,0)
        self.end = end # i.e. (9,9)
        self.is_straight = self._is_straight()

    def __repr__(self) -> str:
        return f"{self.start} -> {self.end}"

    def _is_straight(self):
        return (self.start[0] == self.end[0] or self.start[1] == self.end[1])


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        lines = []
        for row in self.input:
            start, end = parse_input(row)
            lines.append(Line(start, end))
        lines = [line for line in lines if line.is_straight]
        return self.solve_board(lines)

    def part_two(self):
        lines = []
        for row in self.input:
            start, end = parse_input(row)
            lines.append(Line(start, end))
        return self.solve_board(lines)

    def solve_board(self, lines):
        board = Board(lines)
        board.draw_lines()
        return board.count_overlap(2)

RiddleSolver(5).solve()
