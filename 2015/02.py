from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        lines = self.input.split("\n")[:-1]
        demand = 0
        for line in lines:
            m = [int(i) for i in line.split("x")]
            s1 = m[0]*m[1]
            s2 = m[1]*m[2]
            s3 = m[2]*m[0]
            demand += 2*s1 + 2*s2 + 2*s3 + min([s1, s2, s3])
        return demand

    def part_two(self):
        lines = self.input.split("\n")[:-1]
        demand = 0
        for line in lines:
            m = [int(i) for i in line.split("x")]
            m.sort()
            demand += 2*m[0] + 2*m[1] + m[0]*m[1]*m[2]
        return demand


RiddleSolver(2).solve()