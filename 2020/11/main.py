import copy


def read_file(input_file):
    with open(input_file) as file:
        content = file.readlines()
    return [line.strip() for line in content]


def run(seats_overview, too_many_neighbors):
    new_overviews = []
    for overview in seats_overview:
        if overview[0][2] == "L":
            if overview[1].count("#") == 0:
                overview[0][2] = "#"
        elif overview[0][2] == "#":
            if overview[1].count("#") >= too_many_neighbors:
                overview[0][2] = "L"
        new_overviews.append(overview)
    return new_overviews


def print_layout(layout):
    for i in layout:
        print(i)


def count_occupied_seats(layout):
    counter = 0
    for row in layout:
        counter += row.count("#")
    return counter


def update_layout(layout, seats_overview):
    for seat in seats_overview:
        row = seat[0][0]
        col = seat[0][1]
        layout[row] = "".join(layout[row][:col]) + seat[0][2] + "".join(layout[row][col + 1:])
    return layout


def get_overview(layout, func):
    seats_overview = []
    for row_num, row in enumerate(layout):
        for col_num, seat in enumerate(row):
            seat_info = ([row_num, col_num, layout[row_num][col_num]],
                         func(layout, row_num, col_num))
            seats_overview.append(seat_info)
    return seats_overview


def neighbor_seat(layout, seat_row, seat_col):
    top_l, top_m, top_r, lef, rig, bot_l, bot_m, bot_r = "", "", "", "", "", "", "", ""
    width = len(layout[0])
    if seat_row == 0:
        top_l = "."
        top_m = "."
        top_r = "."
    elif seat_row == len(layout) - 1:
        bot_l = "."
        bot_m = "."
        bot_r = "."
    if seat_col == 0:
        top_l = "."
        lef = "."
        bot_l = "."
    elif seat_col == width - 1:
        top_r = "."
        rig = "."
        bot_r = "."
    if not top_l:
        top_l = layout[seat_row - 1][seat_col - 1]
    if not top_m:
        top_m = layout[seat_row - 1][seat_col]
    if not top_r:
        top_r = layout[seat_row - 1][seat_col + 1]
    if not lef:
        lef = layout[seat_row][seat_col - 1]
    if not rig:
        rig = layout[seat_row][seat_col + 1]
    if not bot_l:
        bot_l = layout[seat_row + 1][seat_col - 1]
    if not bot_m:
        bot_m = layout[seat_row + 1][seat_col]
    if not bot_r:
        bot_r = layout[seat_row + 1][seat_col + 1]
    return [top_l, top_m, top_r, lef, rig, bot_l, bot_m, bot_r]


def part_one(input_file, too_many_neighbors):
    layout = read_file(input_file)
    while True:
        seats_overview = get_overview(layout, neighbor_seat)
        new_overview = run(copy.deepcopy(seats_overview), too_many_neighbors)
        if new_overview == seats_overview:
            break
        layout = update_layout(layout, new_overview)
    return count_occupied_seats(layout)


# Part 2
def people_in_sight(layout, seat_row, seat_col):
    top_l, top_m, top_r, lef, rig, bot_l, bot_m, bot_r = "", "", "", "", "", "", "", ""
    width = len(layout[0])
    if seat_row == 0:
        top_l = "."
        top_m = "."
        top_r = "."
    elif seat_row == len(layout) - 1:
        bot_l = "."
        bot_m = "."
        bot_r = "."
    if seat_col == 0:
        top_l = "."
        lef = "."
        bot_l = "."
    elif seat_col == width - 1:
        top_r = "."
        rig = "."
        bot_r = "."

    if not top_l:
        seat_found = False
        index = 1
        while not seat_found and seat_row - index >= 0 and seat_col - index >= 0:
            if layout[seat_row - index][seat_col - index] != ".":
                top_l = layout[seat_row - index][seat_col - index]
                seat_found = True
            index += 1
        if not seat_found:
            top_l = "."

    if not top_m:
        seat_found = False
        index = 1
        while not seat_found and seat_row - index >= 0:
            if layout[seat_row - index][seat_col] != ".":
                top_m = layout[seat_row - index][seat_col]
                seat_found = True
            index += 1
        if not seat_found:
            top_m = "."

    if not top_r:
        seat_found = False
        index = 1
        while not seat_found and seat_row - index >= 0 and seat_col + index <= width - 1:
            if layout[seat_row - index][seat_col + index] != ".":
                top_r = layout[seat_row - index][seat_col + index]
                seat_found = True
            index += 1
        if not seat_found:
            top_r = "."

    if not lef:
        seat_found = False
        index = 1
        while not seat_found and seat_col - index >= 0:
            if layout[seat_row][seat_col - index] != ".":
                lef = layout[seat_row][seat_col - index]
                seat_found = True
            index += 1
        if not seat_found:
            lef = "."

    if not rig:
        seat_found = False
        index = 1
        while not seat_found and seat_col + index <= width - 1:
            if layout[seat_row][seat_col + index] != ".":
                rig = layout[seat_row][seat_col + index]
                seat_found = True
            index += 1
        if not seat_found:
            rig = "."

    if not bot_l:
        seat_found = False
        index = 1
        while not seat_found and seat_row + index <= len(layout) - 1 and seat_col - index >= 0:
            if layout[seat_row + index][seat_col - index] != ".":
                bot_l = layout[seat_row + index][seat_col - index]
                seat_found = True
            index += 1
        if not seat_found:
            bot_l = "."

    if not bot_m:
        seat_found = False
        index = 1
        while not seat_found and seat_row + index <= len(layout) - 1:
            if layout[seat_row + index][seat_col] != ".":
                bot_m = layout[seat_row + index][seat_col]
                seat_found = True
            index += 1
        if not seat_found:
            bot_m = "."

    if not bot_r:
        seat_found = False
        index = 1
        while not seat_found and seat_row + index <= len(
                layout) - 1 and seat_col + index <= width - 1:
            if layout[seat_row + index][seat_col + index] != ".":
                bot_r = layout[seat_row + index][seat_col + index]
                seat_found = True
            index += 1
        if not seat_found:
            bot_r = "."

    return [top_l, top_m, top_r, lef, rig, bot_l, bot_m, bot_r]


def part_two(input_file, too_many_neighbors):
    layout = read_file(input_file)
    while True:
        seats_overview = get_overview(layout, people_in_sight)
        new_overview = run(copy.deepcopy(seats_overview), too_many_neighbors)
        if new_overview == seats_overview:
            break
        layout = update_layout(layout, new_overview)
        # print_layout(layout)
    return count_occupied_seats(layout)


if __name__ == '__main__':
    print(part_one(input_file="input.txt", too_many_neighbors=4))
    print(part_two(input_file="input.txt", too_many_neighbors=5))
