def part_one():
    with open(file="input.txt") as file:
        lines = file.readlines()
        content = [int(x.strip()) for x in lines]
    for i in range(0, len(content)):
        for n in range(i + 1, len(content)):
            if n == len(content):
                continue
            if content[i] + content[n] == 2020:
                return content[i] * content[n]


def part_two():
    with open(file="input.txt") as file:
        lines = file.readlines()
        content = [int(x.strip()) for x in lines]
    for i in range(0, len(content)):
        for n in range(i + 1, len(content)):
            if n == len(content):
                continue
            for m in range(i + 2, len(content)):
                if content[i] + content[n] + content[m] == 2020:
                    return content[i] * content[n] * content[m]


if __name__ == '__main__':
    print(part_one())
    print(part_two())
