def select_bus_line(earliest_time, bus_lines):
    # Would have loved to use list comprehension here but can't use a var defined outside the lc,
    # in this case `earliest_time`
    next_buses = []
    for line in bus_lines:
        next_buses.append((line, line - (earliest_time % line)))
    return min(next_buses, key=lambda x: x[1])


def part_one(input_file):
    with open(input_file) as file:
        content = file.readlines()
    earliest_time = int(content[0].strip())
    bus_lines = [int(line.strip()) for line in content[1].split(",") if not line == "x"]
    closest_bus = select_bus_line(earliest_time, bus_lines)
    return closest_bus[0] * closest_bus[1]


def part_two(input_file):
    with open(input_file) as file:
        bus_lines = file.readlines()[1]
    buses = [(int(line.strip()), i) for i, line in enumerate(bus_lines.split(","))
             if not line == "x"]
    timestamp = 0
    next_step = 1
    for bus in buses:
        while not (timestamp + bus[1]) % bus[0] == 0:
            timestamp += next_step
        next_step *= bus[0]

    return timestamp


if __name__ == '__main__':
    print(part_one(input_file="input.txt"))
    print(part_two(input_file="input.txt"))
