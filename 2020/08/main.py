def read_file(input_file):
    with open(input_file) as file:
        content = file.readlines()
    return [line.strip() for line in content]


class Game:
    def __init__(self, instructions):
        self.accumulator = 0
        self.inp_line_number = 0
        self.hist_inp_line_num = []
        self.loop_detected = False
        self.finished = False
        self.last_line = len(instructions)

    def move(self, inp):
        operation, argument = inp.split()
        if operation == "acc":
            self.acc(argument)
        elif operation == "jmp":
            self.jmp(argument)
        else:
            self.inp_line_number += 1

    def acc(self, inp):
        if self.inp_line_number in self.hist_inp_line_num:
            self.loop_detected = True
            return
        self.hist_inp_line_num.append(self.inp_line_number)
        self.accumulator += int(inp)
        self.inp_line_number += 1
        if self.inp_line_number == self.last_line:
            self.finished = True

    def jmp(self, inp):
        self.inp_line_number += int(inp)
        if self.inp_line_number >= self.last_line:
            self.finished = True


def part_one(input_file):
    instructions = read_file(input_file)
    game = Game(instructions)
    while game.inp_line_number < len(instructions) and not game.loop_detected:
        game.move(instructions[game.inp_line_number])

    return game.accumulator


# Part 2
def modify_instructions(instructions, line_changed):
    new_instructions = instructions.copy()
    for index, line in enumerate(new_instructions[line_changed:]):
        if "jmp" in line:
            line = line.replace("jmp", "nop")
            new_instructions[line_changed + index] = line
            line_changed += index
            return new_instructions, line_changed
        elif "nop" in line:
            line = line.replace("nop", "jmp")
            if line == "jmp +0":
                continue
            new_instructions[line_changed + index] = line
            line_changed += index
            return new_instructions, line_changed
        else:
            continue


def part_two(input_file):
    instructions = read_file(input_file)
    finished = False
    line_changed = -1
    while not finished:
        new_instructions, line_changed = modify_instructions(instructions, line_changed + 1)
        game = Game(new_instructions)
        while game.inp_line_number < len(instructions) and not game.loop_detected:
            game.move(new_instructions[game.inp_line_number])
            finished = game.finished
    return game.accumulator


if __name__ == '__main__':
    print(part_one(input_file="input.txt"))
    print(part_two(input_file="input.txt"))
