from collections import deque


def read_file(input_file):
    with open(input_file) as file:
        content = file.readlines()
    return [line.strip() for line in content]


def part_one(input_file, num_elements):
    inp = read_file(input_file)
    inp = [int(i) for i in inp]
    index = num_elements
    prev_elements = deque(inp[:num_elements], num_elements)
    while index < len(inp):
        found_pair = False
        for i in range(0, num_elements + 1):
            if found_pair:
                break
            if i == num_elements:
                return inp[index]
            for n in range(i + 1, num_elements):
                if prev_elements[i] + prev_elements[n] == inp[index]:
                    found_pair = True
                    break
        prev_elements.append(inp[index])
        index += 1


def part_two(input_file, num_elements):
    inp = read_file(input_file)
    inp = [int(i) for i in inp]
    inv_number = part_one(input_file, num_elements)
    for lower_index in range(0, len(inp)):
        for upper_index in range(lower_index + 2, len(inp)):
            if sum(inp[lower_index:upper_index]) == inv_number:
                return min(inp[lower_index:upper_index]) + max(inp[lower_index:upper_index])


if __name__ == '__main__':
    print(part_one(input_file="input.txt", num_elements=25))
    print(part_two(input_file="input.txt", num_elements=25))
