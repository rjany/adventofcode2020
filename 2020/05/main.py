def read_file(input_file):
    with open(input_file) as file:
        content = file.readlines()
    return [line.strip() for line in content]


def parse_boarding_pass(boarding_pass):
    parsed_pass = {"row": int(boarding_pass[:7].replace("B", "1").replace("F", "0"), 2),
                   "column": int(boarding_pass[7:].replace("R", "1").replace("L", "0"), 2)}
    parsed_pass["id"] = parsed_pass["row"] * 8 + parsed_pass["column"]
    return parsed_pass


def part_one(input_file):
    boarding_passes = read_file(input_file)
    parsed_passes = [parse_boarding_pass(b_pass) for b_pass in boarding_passes]
    return max([p_pass["id"] for p_pass in parsed_passes])


def find_my_id(parsed_passes):
    missing_row = find_missing_row(parsed_passes)
    missing_column = find_missing_column(parsed_passes, missing_row)
    return {"row": missing_row, "column": missing_column, "id": missing_row * 8 + missing_column}


def find_missing_row(parsed_passes):
    rows = range(0, 127)
    for row in rows:
        passes_in_row = [p_pass for p_pass in parsed_passes if p_pass["row"] == row]
        count = len(passes_in_row)
        if 6 < count < 8:
            return row


def find_missing_column(parsed_passes, missing_row):
    columns = range(0, 7)
    list_of_columns = [p_pass["column"] for p_pass in parsed_passes if p_pass["row"] == missing_row]
    return list(set(columns) - set(list_of_columns))[0]


def part_two(input_file):
    boarding_passes = read_file(input_file)
    parsed_passes = [parse_boarding_pass(b_pass) for b_pass in boarding_passes]
    return find_my_id(parsed_passes)


if __name__ == '__main__':
    print(part_one(input_file="input.txt"))
    print(part_two(input_file="input.txt"))
