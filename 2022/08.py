from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        def is_visible_right(x, y, lines):
            return lines[y][x] > max(lines[y][x+1:], default='-1')

        def is_visible_left(x, y, lines):
            return lines[y][x] > max(lines[y][:x], default='-1')

        def is_visible_top(x, y, lines):
            column = [y[x] for y in lines]
            return lines[y][x] > max(column[y+1:], default='-1')

        def is_visible_bottom(x, y, lines):
            column = [y[x] for y in lines]
            return lines[y][x] > max(column[:y], default='-1')

        def is_visible(x, y, lines):
            if 0 in [x, y]:
                return True
            elif len(lines) in [x, y]:
                return True
            return True in [
                is_visible_right(x, y, lines),
                is_visible_left(x, y, lines),
                is_visible_top(x, y, lines),
                is_visible_bottom(x, y, lines)
                ]

        lines = self.input.split("\n")[:-1]
        counter = 0
        for y, row in enumerate(lines):
            for x, _ in enumerate(row):
                if is_visible(x, y, lines):
                    counter += 1
        return counter

    def part_two(self):
        def dist_right(x, y, lines):
            counter = 0
            for h in lines[y][x+1:]:
                counter += 1
                if h >= lines[y][x]:
                    break
            return counter

        def dist_left(x, y, lines):
            counter = 0
            for h in lines[y][:x][::-1]:
                counter += 1
                if h >= lines[y][x]:
                    break
            return counter

        def dist_top(x, y, lines):
            column = [y[x] for y in lines]
            counter = 0
            t = column[:y]
            t.reverse()
            for h in t:
                counter += 1
                if h >= lines[y][x]:
                    break
            return counter

        def dist_bottom(x, y, lines):
            column = [y[x] for y in lines]
            counter = 0
            for h in column[y+1:]:
                counter += 1
                if h >= lines[y][x]:
                    break
            return counter

        def total_distance(x, y, lines):
            right = dist_right(x, y, lines)
            left = dist_left(x, y, lines)
            top = dist_top(x, y, lines)
            bottom = dist_bottom(x, y, lines)
            return  right * left * top * bottom

        lines = self.input.split("\n")[:-1]
        high_score = 0
        for y, row in enumerate(lines):
            for x, _ in enumerate(row):
                current_score = total_distance(x, y, lines)
                if current_score > high_score:
                    high_score = current_score
        return high_score


RiddleSolver(8).solve()  #Adjust the day here.
