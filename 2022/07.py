from aoc_wrapper import BaseRiddleSolver

def recurse(mapping, file_size, directory):
        if directory == "/":
            return
        else:
            mapping[mapping[directory].parent_dir].add_sub_file(file_size)
            recurse(mapping, file_size, mapping[directory].parent_dir)


class Directory():
    def __init__(self, parent_dir):
        self.parent_dir = parent_dir
        self.files = {}
        self.size = 0

    def __repr__(self):
        return f"{self.files}, {self.size}, {self.parent_dir}"

    def add_file(self, name, size):
        self.files[name] = int(size)
        self.size = sum(self.files.values())

    def add_sub_file(self, size):
        self.size += size


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        lines = self.input.split("\n")[:-1]
        mapping = {"/": Directory(None)}
        current_dir = ""
        for line in lines:
            # cd ..
            if line == "$ cd ..":
                current_dir = mapping[current_dir].parent_dir
            # cd x: change currnet directory
            elif line.startswith("$ cd"):
                current_dir = current_dir + line.split(" ")[2]
            # ls: list folder content
            elif line.startswith("$ ls"):
                continue
            # name of dir within current dir: add dir in mapping
            elif line.startswith("dir"):
                name = current_dir + line.split(" ")[1]
                mapping[name] = Directory(current_dir)
            else:
                size, name = line.split(" ")
                mapping[current_dir].add_file(name, size)
                recurse(mapping, int(size), current_dir)
        limit = 100000
        return sum([i.size for i in mapping.values() if i.size < limit])


    def part_two(self):
        lines = self.input.split("\n")[:-1]
        mapping = {"/": Directory(None)}
        current_dir = ""
        for line in lines:
            # cd ..
            if line == "$ cd ..":
                current_dir = mapping[current_dir].parent_dir
            # cd x: change currnet directory
            elif line.startswith("$ cd"):
                current_dir = current_dir + line.split(" ")[2]
            # ls: list folder content
            elif line.startswith("$ ls"):
                continue
            # name of dir within current dir: add dir in mapping
            elif line.startswith("dir"):
                name = current_dir + line.split(" ")[1]
                mapping[name] = Directory(current_dir)
            else:
                size, name = line.split(" ")
                mapping[current_dir].add_file(name, size)
                recurse(mapping, int(size), current_dir)
        limit = 70000000
        required_space = 30000000
        used_space = mapping["/"].size
        available_space = limit - used_space
        to_free_space = required_space - available_space
        diff = (30000000, "")
        for directory in mapping.values():
            # breakpoint()
            if directory.size < to_free_space:
                continue
            else:
                _diff = directory.size - to_free_space
                if _diff < diff[0]:
                    diff = (_diff, directory.size)
        return diff[1]


RiddleSolver(7).solve()  #Adjust the day here.
