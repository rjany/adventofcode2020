from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        lines = self.input.split("\n")[:-1]
        # Get seperator indexes
        separators = [i for i, x in enumerate(lines) if not x]
        calories = []

        for i, separator in enumerate(separators):
            if i == 0:
                calories.append(sum([int(y) for y in lines[:separator]]))
            elif i == len(lines):
                calories.append(sum([int(y) for y in lines[separator:]]))
            else:
                calories.append(sum([int(y) for y in lines[prev_separator + 1:separator]]))
            prev_separator = separator
        return max(calories)

    def part_two(self):
        lines = self.input.split("\n")[:-1]
        # Get seperator indexes
        separators = [i for i, x in enumerate(lines) if not x]
        calories = []

        for i, separator in enumerate(separators):
            if i == 0:
                calories.append(sum([int(y) for y in lines[:separator]]))
            elif i == len(lines):
                calories.append(sum([int(y) for y in lines[separator:]]))
            else:
                calories.append(sum([int(y) for y in lines[prev_separator + 1:separator]]))
            prev_separator = separator
        calories.sort(reverse=True)
        return sum(calories[0:3])


RiddleSolver(1).solve()  #Adjust the day here.
