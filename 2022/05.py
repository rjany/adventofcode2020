from queue import LifoQueue, Queue

from aoc_wrapper import BaseRiddleSolver


class RiddleSolver(BaseRiddleSolver):
    def part_one(self):
        lines = self.input.split("\n")
        stacks_raw = lines[:lines.index("")-1]
        instructions = lines[lines.index("") + 1:-1]
        stacks_raw.reverse()
        stack_count = int(lines[lines.index("")-1][-2])
        stacks_clean = {}
        # Prepare empty stacks
        for _i in range(stack_count):
            stacks_clean[_i+1] = LifoQueue()
        # Fill initial stacks
        for stack in stacks_raw:
            for _i, _ch in enumerate(stack[1::4]):
                if _ch != " ":
                    stacks_clean[_i+1].put(_ch)
        # Execute instructions
        for ins in instructions:
            _amount = int(ins[:ins.index(" from")].split(" ")[1])
            _from = int(ins[ins.index("from"):ins.index(" to")].split(" ")[1])
            _to = int(ins[ins.index("to"):].split(" ")[1])
            for _ in range(_amount):
                stacks_clean[_to].put(stacks_clean[_from].get())     
        solution = ""
        for _i in range(stack_count):
            ch = stacks_clean[_i+1].get()
            solution = solution + ch
        return solution

    def part_two(self):
        lines = self.input.split("\n")
        stacks_raw = lines[:lines.index("")-1]
        instructions = lines[lines.index("") + 1:-1]
        stacks_raw.reverse()
        stack_count = int(lines[lines.index("")-1][-2])
        stacks_clean = {}
        # Prepare empty stacks
        for _i in range(stack_count):
            stacks_clean[_i+1] = LifoQueue()
        # Fill initial stacks
        for stack in stacks_raw:
            for _i, _ch in enumerate(stack[1::4]):
                if _ch != " ":
                    stacks_clean[_i+1].put(_ch)
        # Execute instructions
        for ins in instructions:
            _amount = int(ins[:ins.index(" from")].split(" ")[1])
            _from = int(ins[ins.index("from"):ins.index(" to")].split(" ")[1])
            _to = int(ins[ins.index("to"):].split(" ")[1])
            _temp = LifoQueue()
            for _ in range(_amount):
                _temp.put(stacks_clean[_from].get(timeout=3))
            for _ in range(_amount):
                stacks_clean[_to].put(_temp.get(timeout=3))          
        solution = ""
        for _i in range(stack_count):
            ch = stacks_clean[_i+1].get()
            solution = solution + ch
        return solution


RiddleSolver(5).solve()  #Adjust the day here.
